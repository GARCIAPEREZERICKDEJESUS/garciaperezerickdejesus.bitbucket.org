$(document).ready(function(){
	$('#datetimepicker').datetimepicker({
		timepicker:false,
		format:'Y-m-d'
	});
	obtainGeolocation();

	$('#enviar').on('click',function(){
		var res = new Object();
		var request = new Object();
		var slice = new Object();
		slice.origin = $('#origen').val();
		slice.destination= $('#destino').val();
		slice.date = $('#date').val();
		if($('#escala').is(":checked"))
        {
            slice.maxStops=0;
        }
        
		var arraySlice = new Array();
		arraySlice.push(slice);
		var passengers = new Object();
		if($('#adulto').val()!=0)
		{
			passengers.adultCount = $('#adulto').val();
		}
		else
		{
			passengers.adultCount = 0;
		}
		if($('#nino').val()!=0)
		{
			passengers.childCount = $('#nino').val();
		}
		else
		{
			passengers.childCount = 0;
		}
		passengers.infantInLapCount = 0;
		passengers.infantInSeatCount = 0;
		if($('#adulto_m').val()!=0)
		{
			passengers.seniorCount = $('#adulto_m').val();
		}
		else
		{
			passengers.seniorCount = 0;
		}
			
		request.slice=arraySlice;
		request.passengers=passengers;
		request.solutions=20;
		request.refundable=false;
		res.request=request;
		var myString = JSON.stringify(res);
		console.log(myString);
		
		$.ajax({
			type: 'POST',
			url: 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyBVs3HXaqveBgBFdo-IN8R7CStxk3b5OhQ',
			data: myString,
			contentType:'application/json;charset=utf-8',
			dataType:'json',
			success: function(respuesta){
                tablaVuelos(respuesta);
                // --- Calendar ------------------------------------------------
                if( respuesta.trips.tripOption ){	// Respuesta con datos
                	var _startendtime = document.getElementById('startendtime');
                	var _pdesc = document.getElementById('pdesc');

                	_startendtime.style.display = 'block';
                	_pdesc.style.display = 'block';
                
                	handleClientLoad();	
                }
                
			},
			error: function(){
				alert('Error en la peticion');
			}
		});
	});
	
});

function obtainGeolocation(){
	//obtener la posición actual y llamar a la función  "localitation" cuando tiene éxito
	window.navigator.geolocation.getCurrentPosition(localitation);
}
function localitation(geo){
 	// En consola nos devuelve el Geoposition object con los datos nuestros
 	var latitude = geo.coords.latitude;
 	var longitude = geo.coords.longitude;
	console.log(latitude+","+longitude);

	$.ajax({
		type: 'GET',
		url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&key=AIzaSyBZImKUNBTMob-Ojb-KbTEP-_x2aWtp6dI',
		success: function(data){
			$('#origen').val(data.results[0].address_components[5].short_name.toUpperCase().substr(0,3));
		},
		error: function(){
			alert('Error en la peticion');
		}
	});
}

function graficaPrecios(obj)
{
    var arrayGrafica_adulto=[];
	var arrayGrafica_adulto_m=[];
	var arrayGrafica_nino=[];
	arrayGrafica_adulto.push(new Array('Vuelo','Precio MXN'));
	arrayGrafica_adulto_m.push(new Array('Vuelo','Precio MXN'));
	arrayGrafica_nino.push(new Array('Vuelo','Precio MXN'));
	var tripOpt=obj.trips.tripOption;
	var aerolinea=obj.trips.data.carrier;
	//OPCIONES DE LA GRAFICA
	var options = {
		width: 1200,
		legend: { position: 'none' },
		chart: { subtitle: 'precios en MXN ' },
		axes: {
		   	x: {
		       	0: { side: 'top', label: 'VUELOS PARA EL '+ $('#date').val()} // Top x-axis.
		    }
		},
		bar: { groupWidth: "50%" }
	};
	$.each(tripOpt,function(i, value){
		$.each(tripOpt[i].pricing,function(k, value3){
			$.each(aerolinea,function(j,value2){
				if(tripOpt[i].slice[0].segment[0].flight.carrier==aerolinea[j].code)
				{
					if(tripOpt[i].pricing[k].passengers.hasOwnProperty('adultCount'))
					{
						arrayGrafica_adulto.push(new Array(aerolinea[j].name+' '+tripOpt[i].slice[0].segment[0].flight.number, (tripOpt[i].pricing[k].passengers.adultCount*parseInt(tripOpt[i].pricing[k].saleTotal.substr(3))).toString() )) ;
					}
					if(tripOpt[i].pricing[k].passengers.hasOwnProperty('seniorCount'))
					{
						arrayGrafica_adulto_m.push(new Array(aerolinea[j].name+' '+tripOpt[i].slice[0].segment[0].flight.number,(tripOpt[i].pricing[k].passengers.seniorCount*parseInt(tripOpt[i].pricing[k].saleTotal.substr(3))).toString() ));
						
					}
					if(tripOpt[i].pricing[k].passengers.hasOwnProperty('childCount'))
					{
						arrayGrafica_nino.push(new Array(aerolinea[j].name+' '+tripOpt[i].slice[0].segment[0].flight.number,(tripOpt[i].pricing[k].passengers.childCount*parseInt(tripOpt[i].pricing[k].saleTotal.substr(3))).toString() ));
						
					}
					
				}
			});			
		});
	});
	if(arrayGrafica_adulto.length>1)
	{
		$('#precios_adulto').show();
		if($('#adulto').val()>1)
		{
			options.title='PRECIOS PARA '+$('#adulto').val()+' ADULTOS';
		}
		else
		{
			options.title='PRECIOS PARA 1 ADULTO';
		}
		var datos=new google.visualization.arrayToDataTable(arrayGrafica_adulto);
		var chart = new google.charts.Bar(document.getElementById('precios_adulto'));
		
		chart.draw(datos, google.charts.Bar.convertOptions(options));
	}
	else
	{
		$('#precios_adulto').hide();
	}
	if(arrayGrafica_adulto_m.length>1)
	{
		$('#precios_adulto_m').show();
		if($('#adulto').val()>1)
		{
			options.title='PRECIOS PARA '+$('#adulto_m').val()+' ADULTOS MAYORES';
		}
		else
		{
			options.title='PRECIOS PARA 1 ADULTO MAYOR';
		}
		var datos=new google.visualization.arrayToDataTable(arrayGrafica_adulto_m);
		var chart = new google.charts.Bar(document.getElementById('precios_adulto_m'));
		
		chart.draw(datos, google.charts.Bar.convertOptions(options));
	}
	else
	{
		$('#precios_adulto_m').hide();
	}
	if(arrayGrafica_nino.length>1)
	{
		$('#precios_nino').show();
		if($('#nino').val()>1)
		{
			options.title='PRECIOS PARA '+$('#nino').val()+' NI\u00D1OS';
		}
		else
		{
			options.title='PRECIOS PARA 1 NI\u00D1O';
		}
		var datos=new google.visualization.arrayToDataTable(arrayGrafica_nino);
		var chart = new google.charts.Bar(document.getElementById('precios_nino'));
		
		chart.draw(datos, google.charts.Bar.convertOptions(options));
	}
	else
	{
		$('#precios_nino').hide();
	}
	
}
function tablaVuelos(vuelos)
{
	console.log(vuelos);
    var data = new google.visualization.DataTable();
        data.addColumn('string', 'N\u00FAmero de vuelo');
        data.addColumn('string', 'Aerol\u00EDnea');
        data.addColumn('string', 'Hora de salida');
        data.addColumn('string', 'Hora de llegada');
        data.addColumn('string', 'Precio (MXN)');
    var arrayVuelos=[];
    var tripOpt=vuelos.trips.tripOption;
    var aerolinea=vuelos.trips.data.carrier;
    $.each(tripOpt,function(i,value){
        $.each(aerolinea,function(j,value2){
            if(tripOpt[i].slice[0].segment[0].flight.carrier==aerolinea[j].code)
            {
                arrayVuelos.push(new Array(tripOpt[i].slice[0].segment[0].flight.number, aerolinea[j].name, tripOpt[i].slice[0].segment[0].leg[0].departureTime.substr(11,15),tripOpt[i].slice[0].segment[0].leg[0].arrivalTime.substr(11,15), '$'+tripOpt[i].saleTotal.substr(3)));
            }
        });
    });
    data.addRows(arrayVuelos);
    var table = new google.visualization.Table(document.getElementById('table_div'));
        table.draw(data, {showRowNumber: true, width: '100%', height: '50%'});
}
function graficaAerolineas(aerolineas)
{
    var arrayKilos=[];
    var arrayCantidad=[];
    arrayKilos.push(new Array('Vuelo','Kilos'));
    var tripOpt=aerolineas.trips.tripOption;
    var aerolinea=aerolineas.trips.data.carrier;
   
    $.each(aerolinea,function(j,value2){
        arrayCantidad[j]=0;
        $.each(tripOpt,function(i, value){
            if(tripOpt[i].slice[0].segment[0].flight.carrier==aerolinea[j].code)
            {
                arrayCantidad[j]++;
            }
        });
        arrayKilos.push(new Array(aerolinea[j].name, arrayCantidad[j]));
    });
    var data = google.visualization.arrayToDataTable(arrayKilos);
    var options = {
          title: 'Cantidad de aerolineas con vuelos para la fecha '+$('#date').val(),
          is3D: true,
    };
    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
}