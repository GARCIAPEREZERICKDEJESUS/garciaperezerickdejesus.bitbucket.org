var XHR=(function(){
    var _detectar_cambios_de_estado=function(xhr,_obj_parametros){
        return function(){
            if(xhr.readyState===4){
                if(xhr.status >=200 && xhr.status<=299){
                    console.log("exito");
                    _obj_parametros.en_caso_de_exito(xhr.responseText);
                }
                else{
                    console.log("error");
                    _obj_parametros.en_caso_de_error(xhr.responseText);
                }
            }
        };
    };
var _get=function(_obj_parametros){
    var xhr= new XMLHttpRequest();
    xhr.onreadystatechange=_detectar_cambios_de_estado(xhr,_obj_parametros);
  var url=_obj_parametros.url;
    xhr.open("get",url);
    xhr.send();
};
return{

"get":_get
};




})();